package com.mytaxi.android_demo;


import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.mytaxi.android_demo.activities.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class AuthenticationActivityInstrumentedTest {
    String username, password;
    String username1, password1;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Before
    public void initLoginCredentials() {
        // Specify a valid string
        username = "crazydog335";
        password = "venture";
        username1 = "don";
        password1 = "password";
    }

    @Test
    public void testValidLoginSuccess() {
        // Type text and then press the button.
        onView(withId(R.id.edt_username))
                .perform(typeText(username));
        onView(withId(R.id.edt_password))
                .perform(typeText(password));
        onView(withId(R.id.btn_login))
                .perform(click());

        // Check that the textSearch field is present.
        onView(withId(R.id.textSearch))
               .check(matches(isDisplayed()));
    }

    @Test
    public void testFailedLogin() {
        // Type text and then press the button.
        onView(withId(R.id.edt_username))
                .perform(clearText());
        onView(withId(R.id.edt_password))
                .perform(clearText());
        onView(withId(R.id.btn_login))
                .perform(click());

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.message_login_fail)))
                .check(matches(isDisplayed()));
    }

    @Before
    @Test
    public void testLogout() {
        onView(withId(R.id.nav_logout)).perform(click());

        onView(withId(R.id.btn_login))
                .check(matches(isDisplayed()));
    }

}




